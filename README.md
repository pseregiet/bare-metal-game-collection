## Bare Metal Game Collection
![The resoult](http://i.imgur.com/ISX75tc.gif  "Game Selection")
[Watch it on real hardware](https://www.youtube.com/watch?v=7Mmwdpzy4g0) 

4 simple games, Snake, Pong, Arkanoid and Tetris. I coded those games in C + SDL before some time ago and I wanted to push it to the next (or previous, depends how you look at it) level. It is supposed to work on ALL, or at least the vast majority of computers, so I am using only the features that are guaranteed to be supported. That, unfortunately, excluseds the Vsync interrupt and changing the default colors of VGA...

This program crashes on my Pentium1 laptop after the "C64 loader", that's why there are still no games, I need to make it work first. To make testing easier on that ancient hardware I also coded a DOS loader, which takes the floppy image and copies it to the specific address, where the BIOS would load it when booting. It is the "loader.S" file and it is not necessary to compile the "Bare Metal Game Collection". 

There are 4 scripts included
#### c.sh
Compiles the Game Collection.
#### cr.sh
Compiles the Game Collection and loads it in QEMU emulator.
#### debug.sh
Loads the Game Collection into QEMU emulator and connects GDB debugger to it. It will automatically set up a breakpoint at the entry point.
#### ccom.sh
Compiles the DOS loader and runs dosbox

##### This code is under GNU v2 license. See LICENSE file for more information.

Patryk Seregiet 2017